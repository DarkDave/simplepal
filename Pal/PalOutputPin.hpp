/*
 * PalOutputPin.hpp
 *
 *  Created on: Apr 2, 2020
 *      Author: dave
 */

#ifndef PALOUTPUTPIN_HPP_
#define PALOUTPUTPIN_HPP_

#include "../Pal/PalInputPin.hpp"

class PalOutputPin: public virtual PalInputPin {
public:
	PalOutputPin();
	virtual ~PalOutputPin();

	virtual void toggle() = 0;
	virtual void write(const bool state) = 0;
};

#endif /* PALOUTPUTPIN_HPP_ */
