/*
 * PalUart.hpp
 *
 *  Created on: Apr 2, 2020
 *      Author: dave
 */

#ifndef PALUART_HPP_
#define PALUART_HPP_

#include <string>

class PalUart {
public:
	PalUart();
	virtual ~PalUart();

	virtual void initialize() = 0;
	virtual std::string receive() = 0;
	virtual void send(const std::string &message) = 0;
};

#endif /* PALUART_HPP_ */
