/*
 * PalInputPin.hpp
 *
 *  Created on: Apr 2, 2020
 *      Author: dave
 */

#ifndef PALINPUTPIN_HPP_
#define PALINPUTPIN_HPP_

class PalInputPin {
public:
	PalInputPin();
	virtual ~PalInputPin();

	virtual bool read() const = 0;
};

#endif /* PALINPUTPIN_HPP_ */
