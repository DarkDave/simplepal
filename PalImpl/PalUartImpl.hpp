/*
 * PalUartImpl.hpp
 *
 *  Created on: Apr 2, 2020
 *      Author: dave
 */

#ifndef PALUARTIMPL_HPP_
#define PALUARTIMPL_HPP_

#include "../Pal/PalUart.hpp"

extern "C" {
#include "stm32l1xx_hal.h"
}

class PalUartImpl : public PalUart {
public:
	PalUartImpl(UART_HandleTypeDef *pUartHandle);
	~PalUartImpl();

	void initialize() override;
	std::string receive() override;
	void send(const std::string &message) override;

private:
	UART_HandleTypeDef *pUartHandle;
	const uint32_t Timeout = 10000;
	const uint16_t MaxReceiveDataSize = 1000;
};

#endif /* PALUARTIMPL_HPP_ */
