/*
 * PaiInputPinImpl.hpp
 *
 *  Created on: Apr 2, 2020
 *      Author: dave
 */

#ifndef PALINPUTPINIMPL_HPP_
#define PALINPUTPINIMPL_HPP_

#include "../Pal/PalInputPin.hpp"

extern "C" {
#include "stm32l1xx_hal.h"
}

class PalInputPinImpl : public virtual PalInputPin {
public:
	PalInputPinImpl(GPIO_TypeDef *gpio, const uint16_t pin);
	~PalInputPinImpl();

	bool read() const override;

private:
	GPIO_TypeDef *Gpio;
	const uint16_t Pin;
};

#endif /* PALINPUTPINIMPL_HPP_ */
