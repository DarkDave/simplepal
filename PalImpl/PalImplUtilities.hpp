/*
 * PalImplUtilities.hpp
 *
 *  Created on: Apr 4, 2020
 *      Author: dave
 */

#ifndef PALIMPLUTILITIES_HPP_
#define PALIMPLUTILITIES_HPP_

extern "C" {
#include "stm32l1xx_hal.h"
}

inline bool GPIO_PinState2Bool(const GPIO_PinState state) {
	return (state == GPIO_PIN_SET) ? true : false;
}

inline GPIO_PinState bool2GPIO_PinState(const bool state) {
	return (state) ? GPIO_PIN_SET :  GPIO_PIN_RESET;
}

#endif /* PALIMPLUTILITIES_HPP_ */
