/*
 * PalOutputPinImpl.hpp
 *
 *  Created on: Apr 2, 2020
 *      Author: dave
 */

#ifndef PALOUTPUTPINIMPL_HPP_
#define PALOUTPUTPINIMPL_HPP_

#include <stdint.h>

#include "PalInputPinImpl.hpp"
#include "../Pal/PalOutputPin.hpp"

extern "C" {
#include "stm32l1xx_hal.h"
}

class PalOutputPinImpl : public virtual PalOutputPin,  public PalInputPinImpl {
public:
	PalOutputPinImpl(GPIO_TypeDef *gpio, const uint16_t pin);
	~PalOutputPinImpl();

	void toggle() override;
	void write(const bool state) override;

private:
	GPIO_TypeDef *Gpio;
	const uint16_t Pin;
};

#endif /* PALOUTPUTPINIMPL_HPP_ */
